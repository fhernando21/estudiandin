/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author fherv
 */
public class Palabra implements Comparable<Palabra> {
    private Integer frec;
    private String Key;
    
    public Palabra(Integer frec, String cars) {
        this.frec = frec;
        this.Key = cars;
    }
    
    
    
    public Palabra() {
    }

    public Integer getFrec() {
        return frec;
    }

    public void setFrec(int frec) {
        this.frec = frec;
    }

    public String getCars() {
        return Key;
    }

    public void setCars(String cars) {
        this.Key = cars;
    }
    @Override
    public String toString() {
        return "Palabra{" + "frec=" + frec + ", Key=" + Key + '}';
    }
    @Override
    public int compareTo(Palabra p) {
        return frec.compareTo(p.getFrec()); 
    }

  
    
    
    
}
